package com.xu.utils;

import java.sql.*;
import java.util.ResourceBundle;

public class DBUtils {
    private static String driver;
    private static String url;
    private static String username;
    private static String password;

    static{//获取数据，加载驱动
        //获取db文件
        ResourceBundle bundle = ResourceBundle.getBundle("db");
        //加载数据
        driver = bundle.getString("driver");
        url = bundle.getString("url");
        username = bundle.getString("username");
        password = bundle.getString("password");
        //jdbc连接数据库
        try {
            //Class.forName是初始化给定的类 这样就能分辨数据库类型 初始化对应数据库的类
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static Connection  getConnection(){
        Connection conn = null;
        try {
            //获取数据库的连接
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
   //一个关闭功能 用来关闭中间产生的ResultSet Statement Connection
    public static void close(ResultSet rs, Statement stmt,Connection conn){
        if(rs!=null){
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(stmt!=null){
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(conn!=null){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

}
