package com.xu.dao;

import com.xu.bean.Goods;

import java.util.List;

public interface GoodsDao {
    public List<Goods> getgoods();
}
