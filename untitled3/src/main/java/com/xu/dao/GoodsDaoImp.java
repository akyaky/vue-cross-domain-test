package com.xu.dao;

import com.xu.bean.Goods;
import com.xu.utils.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GoodsDaoImp implements GoodsDao {
    public List<Goods> getgoods() {
        //List集合存放Goods类型 多态
        List<Goods> recordList = new ArrayList<Goods>();
        //得到数据库连接conn
        Connection conn = DBUtils.getConnection();
        try {
            //PreparedStatement用来进行一次性操作连接
            PreparedStatement ps = conn.prepareStatement("select * from goods");
            //将得到的结果返回给ResultSet类型的rs
            ResultSet rs= ps.executeQuery();
            //遍历一下把rs里的数据放入recordList
            while(rs.next()){
                Goods g=new Goods();
                g.setName(rs.getString("name"));
                g.setUrl(rs.getString("picture"));
                g.setPrice(rs.getString("Price"));
                recordList.add(g);
            }
            //关闭连接
            DBUtils.close(rs,ps,conn);
            //放回一下recordList数据
            return recordList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

