package com.xu.web;

import com.alibaba.fastjson.JSONObject;
import com.xu.bean.Goods;
import com.xu.dao.GoodsDao;
import com.xu.dao.GoodsDaoImp;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
//注解 如果地址是/adv则读取信息
@WebServlet(urlPatterns = "/adv")
public class LoginServlet extends HttpServlet {
    //重写doGet方法
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //头信息
        response.setContentType("application/json;charset=utf-8");

        //这里new一个Goods
        Goods good=new Goods();
        try {
            //将request中的参数映射到User Bean中的set和get方法
            BeanUtils.populate(good,request.getParameterMap());
            //创建类接口调用方法获取数据
            GoodsDao gs=new GoodsDaoImp();
            //将数据库获取的数据给List集合
            List<Goods> recordList=gs.getgoods();
            //写入到网页
            PrintWriter writer = response.getWriter();
            String  jsonObject = JSONObject.toJSON(recordList).toString();
            writer.write(jsonObject);
            //控制台输出测试
            System.out.println("json字符串"+jsonObject);
//            if(u!=null){//分发转发
//                request.getSession().setAttribute("user",u);
////              response.sendRedirect(request.getContextPath()+"/home.html");
//                //http://localhost:9000/mvc_web/
//                response.sendRedirect(request.getContextPath()+"/home.jsp");
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //默认不重写
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
