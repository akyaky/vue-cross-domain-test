/*
 Navicat Premium Data Transfer

 Source Server         : ces
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : shopping

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 30/04/2022 20:41:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Price` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('小米12', 'https://2e.zol-img.com.cn/product/217_320x240/872/ceaXJc9JlTdk.jpg', '3999');
INSERT INTO `goods` VALUES ('小米11', 'https://2e.zol-img.com.cn/product/210_320x240/792/ceveQccXbewx2.jpg', '3299');
INSERT INTO `goods` VALUES ('一加9RT', 'https://2c.zol-img.com.cn/product/217_320x240/970/ceUNNW2KxrwE.jpg', '4999');
INSERT INTO `goods` VALUES ('华为P50', 'https://2e.zol-img.com.cn/product/217_320x240/206/cea1MyCmAU3w.jpg', '8988');
INSERT INTO `goods` VALUES ('苹果iPhone 13 Pro Max', 'https://2a.zol-img.com.cn/product/215_320x240/44/ce88e6G9caJg.jpg', '8999');

SET FOREIGN_KEY_CHECKS = 1;
